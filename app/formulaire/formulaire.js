const remote = require('electron').remote;

// ===== Evenement sur le lien closeWindow =====
document.querySelector(`#close-window`).addEventListener('click', () => window.close())

// ===== Fonctions de récuperation de donnée du formulaire =====
document.querySelector('#button-validation').addEventListener('click', () => {

    let newElements = {
        name: document.querySelector(`input#nom`).value,
        rental: document.querySelector(`input#location`).checked,
        sale: document.querySelector(`input#vente`).checked,
        city: document.querySelector(`input#ville`).value,
        size: document.querySelector(`input#superficie`).value,
        price: document.querySelector(`input#prix`).value,
        description: document.querySelector(`input#description`).value,
        linkPicture: document.querySelector(`input#lien-photo`).value,
        booked: document.querySelector(`input#reserve`).checked,
    }

    let arrayElements = [];

    if(localStorage.getItem('elements') !== null){
        arrayElements = JSON.parse(localStorage.getItem('elements'))
    }

    if((newElements.name && newElements.city && newElements.size && newElements.price) !== ""){
        arrayElements.push(newElements);

        localStorage.setItem('elements', JSON.stringify(arrayElements));
    
        remote.getCurrentWindow().getParentWindow().send('element-added')
        
        window.close()
    }
})

