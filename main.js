// ===== Variables et appelle framework electron =====
const {BrowserWindow, app} = require('electron');

// ===== Fonction de création de la fenetre principal =====
function createWindow () {

    const win = new BrowserWindow({
        width: 1024,
        height: 768,
        maxWidth: 1024,
        minWidth: 1024,
        maxHeight: 768,
        minHeight: 768,
        webPreferences: {
            nodeIntegration: true
        },
        resizable: false
    });

    win.loadFile(`app/index/index.html`);

}

// ===== Methode d'ouverture d'application =====
app.whenReady().then(createWindow);